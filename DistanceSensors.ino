#include <Wire.h>
#include <VL53L0X.h>

VL53L0X sensor;
VL53L0X sensor2;
VL53L0X sensor3;
VL53L0X sensor4;

int distance = 0;
int distance2 = 0;
int distance3 = 0;
int distance4 = 0;


void setup() {
  // put your setup code here, to run once:

  pinMode(18, OUTPUT);
  digitalWrite(18, LOW);
  pinMode(17, OUTPUT);
  digitalWrite(17, LOW);
  pinMode(16, OUTPUT);
  digitalWrite(16, LOW);
  pinMode(15, OUTPUT);
  digitalWrite(15, LOW);

   delay(500);

  Wire.begin();

  Serial.begin (9600);

  pinMode(15, INPUT);
  delay(150);
  Serial.println("00");
  sensor.init(true);
  Serial.println("01");
  delay(100);
  sensor.setAddress((uint8_t)22);
  Serial.println("02");

  pinMode(16, INPUT);
  delay(150);
  Serial.println("03");
  sensor2.init(true);
  Serial.println("04");
  delay(100);
  sensor2.setAddress((uint8_t)25);
  Serial.println("05");

  pinMode(17, INPUT);
  delay(150);
  Serial.println("06");
  sensor3.init(true);
  Serial.println("07");
  delay(100);
  sensor3.setAddress((uint8_t)28);
  Serial.println("08");

  pinMode(18, INPUT);
  delay(150);
  Serial.println("09");
  sensor4.init(true);
  Serial.println("10");
  delay(100);
  sensor4.setAddress((uint8_t)31);
  Serial.println("11");

  Serial.println("addresses set");

  Serial.println ("I2C scanner. Scanning ...");
  byte count = 0;


  for (byte i = 1; i < 120; i++)
  {

    Wire.beginTransmission (i);
    if (Wire.endTransmission () == 0)
    {
      Serial.print ("Found address: ");
      Serial.print (i, DEC);
      Serial.print (" (0x");
      Serial.print (i, HEX);
      Serial.println (")");
      count++;
      delay (1);  // maybe unneeded?
    } // end of good response
  } // end of for loop
  
  Serial.println ("Done.");
  Serial.print ("Found ");
  Serial.print (count, DEC);
  Serial.println (" device(s).");

  delay(1000);


  sensor.setTimeout(500);
  sensor2.setTimeout(500);
  sensor3.setTimeout(500);
  sensor4.setTimeout(500);


  sensor.startContinuous();
  sensor2.startContinuous();
  sensor3.startContinuous();
  sensor4.startContinuous();

}

void loop() {
  // put your main code here, to run repeatedly:

  distance = sensor.readRangeContinuousMillimeters(); // worked out from datasheet graph  Left Sensor
  distance2 = sensor2.readRangeContinuousMillimeters(); // worked out from datasheet graph  Back Sensor
  distance3 = sensor3.readRangeContinuousMillimeters(); // worked out from datasheet graph  Right Sensor
  distance4 = sensor4.readRangeContinuousMillimeters(); // worked out from datasheet graph  Front Sensor

  delay(500);

  Serial.println(distance);
  Serial.println(distance2);
  Serial.println(distance3);
  Serial.println(distance4);

}
